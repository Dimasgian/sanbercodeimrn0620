console.log('No.1');

class Animal {
    constructor(jenis) {
        this._name = jenis;
        this._legs = 4;
        this._cold_blooded = false;
    }
    get name() {
        return this._name;
    }
    get legs() {
        return this._legs;
    }
    get cold_blooded() {
        return this._cold_blooded;
    }
}

var sheep = new Animal("shaun");
 
console.log('==========');
console.log('Release 0');
console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

class Ape extends Animal {
    constructor(jenis) {
        super(jenis);
        this._legs = 2;
    }
    yell() {
        console.log('Auooo')
    }
}

class Frog extends Animal {
    constructor(jenis) {
        super(jenis);
        this._legs = 2;
    }
    jump() {
        console.log('hop hop')
    }
}

console.log('==========');
console.log('Release 1');

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log(' ');

console.log('No. 2');

class Clock({ template }) {

    constructor({ template }) {

        this.template = template;
    }

    render() {

        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
    
        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    };
  
    start() {
        this.render();
        this.timer = setInterval(()=>this.render, 1000);
    };
    
}

var clock = new Clock({template: 'h:m:s'});

clock.start();  

function Clock({ template }) {
  
    var timer;
  
    function render() {
        
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
      
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  
  }

  var clock = new Clock({template: 'h:m:s'});

clock.start();  



