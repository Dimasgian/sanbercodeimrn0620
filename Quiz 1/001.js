/*
  A. Bandingkan Angka (10 poin)
    Buatlah sebuah function dengan nama bandingkan() yang menerima sebuah parameter berupa number 
    dan bilangan asli (positif). Jika salah satu atau kedua paramater merupakan bilangan negatif 
    maka function akan mereturn -1. Function tersebut membandingkan kedua parameter 
    dan mereturn angka yang lebih besar di antara keduanya. Jika kedua parameter sama besar 
    maka function akan mereturn nilai -1. 

  B. Balik String (10 poin)
    Diketahui sebuah function balikString yang menerima satu buah parameter berupa tipe data string. Function balikString akan mengembalikan sebuah string baru yang merupakan string kebalikan dari parameter yang diberikan. contoh: balikString("Javascript") akan me-return string "tpircsavaJ", balikString("satu") akan me-return string "utas", dst.

    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 

  C. Palindrome (10 poin)
    Buatlah sebuah function dengan nama palindrome() yang menerima sebuah parameter berupa String. 
    Function tersebut mengecek apakah string tersebut merupakan sebuah palindrome atau bukan. 
    Palindrome yaitu sebuah kata atau kalimat yang jika dibalik akan memberikan kata atau kalimat yang sama. 
    Function akan me-return tipe data boolean:  true jika string merupakan palindrome, dan false jika string bukan palindrome. 

  
    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 
  
    
*/

console.log("Jawab no.1")

var num1;
var num2;

function bandingkan(num1, num2) {

    if(num1 < num2){
        return num2;
    }

    else if(num1 > num2){
        return num1;
    }

    else if(num1 == num2){
        return -1;
    }

    else if(num1 == 1 || num2 == undefined){
        return 1;
    }

    else{
        return -1;
    }

}
console.log(bandingkan(10, 15));
console.log(bandingkan(12, 12));
console.log(bandingkan(-1, 10));  
console.log(bandingkan(112, 121));
console.log(bandingkan(1)); 
console.log(bandingkan()); 
console.log(bandingkan("15", "18"))

console.log("\n")
console.log("Jawab no.2")

function balikString(str) {
  
      var newString = " ";
   
      for (var a = str.length - 1; a >= 0; a--) { 

          newString += str[a];
      }
      return newString;
  } 
  console.log(balikString("abcde")) 
  console.log(balikString("rusak")) 
  console.log(balikString("racecar")) 
  console.log(balikString("haji")) 



  console.log("\n")
  console.log("Jawab no.3")

  function palindrome(sr) {
    var strnew =""
   
  for (var a = sr.length - 1; a >= 0; a--) { 

    strnew += sr[a];
  }
      if (strnew==sr){
        return "true"
      }
      else {
        return "else"
      }
  

}
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false





