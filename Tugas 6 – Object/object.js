console.log(' ')
console.log('No. 1')

function arrayayToObject(array) {
    
    var now = new Date();
    var thisYear = now.getFullYear(); // 2020 (year sekarang)

    if(array.length == 0) {

        console.log('""');
    } 

    else {

        for(var i = 0; i < 2; i++) {

            var age;
        
            if(array[i][3] === null || array[i][3] == undefined || array[i][3] > thisYear) {

                var year = array[i][3];
                age = thisYear - year
                console.log(i + 1 + '. ' + array[i][0] + ' ' + array[i][1])
                
                var data = {};
                data.firstName = array[i][0];
                data.lastName  = array[i][1];
                data.gender    = array[i][2];
                data.age       = 'Invalid Birth Year';

                console.log(data);
            } 

            else {

                age = thisYear - array[i][3];

                console.log(i + 1 + '. ' + array[i][0] + ' ' + array[i][1]);

                var data = {};
                data.firstName = array[i][0];
                data.lastName  = array[i][1];
                data.gender    = array[i][2];
                data.age       = age;

                console.log(data);
            }
        }
    }
}
 
// Driver Code
console.log(' ');
var people = [ 
                ['Bruce', 'Banner', 'male', 1975], 
                ['Natasha', 'Romanoff', 'female' ] 
             ];

arrayayToObject(people);

console.log(' ');

var people2 = [ 
                ['Tony', 'Stark', 'male', 1980], 
                ['Pepper', 'Pots', 'female', 2023] 
              ];
arrayayToObject(people2);

console.log(' ');

// Error case 
arrayayToObject([]); 

console.log(' ');
console.log('No. 2');

var money;

function shoppingTime(memberId, money) {

    var products = [];
    var member = memberId;
    var cash  = money;

    for(i = 0; i < 4; i++) {

        if(memberId == '' || money == undefined) {

            return 'Mohon maaf, toko X hanya berlaku untuk member saja';
        }

        else if(money < 50000) {

            return 'Mohon maaf, uang tidak cukup';
        }

        else { 

            if(money > 50000) {

                //Casing
            products.push('Casing Handphone');
            cash -= 50000;

                if(cash >= 175000) {

                    //Sweater
                products.push('Sweater Uniklooh');
                cash -= 175000;

                    if(cash >= 250000) {

                        //Baju H&N
                    products.push('Baju H&N');
                    cash -= 250000;
                        
                        if(cash >= 500000) {
                            //Baju Zoro
                        products.push('Baju Zoro');
                        cash -= 500000;

                            if(cash >= 1500000) {

                                //Sepatu
                            products.push('Sepatu Stacattu');
                            cash -= 1500000;

                            var belanja = {};
                            belanja.memberId      = memberId;
                            belanja.money         = money;
                            belanja.listPurchased = products;
                            belanja.changeMoney   = cash;
                            return belanja;
                            }   
                        } 

                        else {

                            //Sweater Unikloh
                        var belanja = {};
                        belanja.memberId      = memberId;
                        belanja.money         = money;
                        belanja.listPurchased = products;
                        belanja.changeMoney   = cash;
                        return belanja;
                        }
                    } 

                    else {

                        //Baju H&N
                    var belanja = {};
                    belanja.memberId      = memberId;
                    belanja.money         = money;
                    belanja.listPurchased = products;
                    belanja.changeMoney   = cash;
                    return belanja;
                    }
                } 

                else {

                    //Baju Zoro
                var belanja = {};
                belanja.memberId      = memberId;
                belanja.money         = money;
                belanja.listPurchased = products;
                belanja.changeMoney   = cash;
                return belanja;

                }   
            } 
        }
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));

//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));

//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); //Mohon maaf, toko X hanya berlaku untuk member saja

console.log(' ')

console.log('No. 3')

function naikAngkot(arrayPenumpang) {
    var ArryPen = arrayPenumpang;
    var ArrayJaw = [];
    var index1;
    var index2;
    var hitung;
    var harga;

    rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    if(ArryPen.length == 0) {

        return '""';
    } 

    else { 

        for(var i = 0; i < 2; i++) {

            index1 = rute.indexOf(ArryPen[i][1]);
            index2 = rute.indexOf(ArryPen[i][2]);
            hitung = index2 - index1;
            harga  = hitung * 2000;
    
            var user = {};
            user.penumpang = ArryPen[i][0];
            user.naikDari  = ArryPen[i][1];
            user.tujuan    = ArryPen[i][2];
            user.bayar     = harga;
    
            ArrayJaw.push(user);
        } 

        return ArrayJaw;

    }
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
console.log(' ');

console.log(naikAngkot([]));


