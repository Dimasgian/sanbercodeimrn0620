// Tugas 1
console.log("Jawab no.1")

 var startnum
 var finishnum

 function range(startnum,finishnum) {
     var array =[]
     var i
     if (startnum<finishnum) {

         for(i=startnum;i<=finishnum;i++){

         array.push(i)

         }
     }
  
    else if (startnum>finishnum){

     for(i=startnum;i>=finishnum;i--){

         array.push(i)

        }
    }
  
    else{
        array=console.log(-1)
    }
    return array
 }
 console.log(range(1, 10)) 
 console.log(range(1)) 
 console.log(range(11,18)) 
 console.log(range(54, 50)) 
 console.log(range())

 console.log("\n")

// Tugas 2
console.log("Jawab no.2")

var step

function rangeWithStep(startNum, finishNum, step) {

    var array2 = []

    if (startNum < finishNum) {
        for(i = startNum; i <= finishNum; i = i + step) {
            array2.push(i)
        }
    }

    else if (startNum > finishNum) {
        for(i = startNum; i >= finishNum; i = i - step) {
            array2.push(i)
        }
    }

    return array2
}
console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4)) 

console.log("\n")


// Tugas 3
console.log("Jawab no.3")

function rangeWithStep(startNum, finishNum, step) {

    var array3 = []

    if (startNum < finishNum) {
        for(i = startNum; i <= finishNum; i = i + step) {
            array3.push(i)
        }
    }

    else if (startNum > finishNum) {
        for(i = startNum; i >= finishNum; i = i - step) {
            array3.push(i)
        }
    }

    return array3
}

console.log("\n")

// Tugas 4
console.log("Jawab no.4")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling() {
    for(var baris = 0; baris < 4; baris++) {
        for(var kolom = 0; kolom < 5; kolom++) {
            if (kolom == 0) {
                console.log("Nomor ID: " + input[baris][kolom])
            }

            else if (kolom == 1) {
                console.log("Nama Lengkap: " + input[baris][kolom])
            }

            else if (kolom == 3) {
                console.log("TTL: " + input[baris][kolom])
            }

            else if (kolom == 4) {
                console.log("Hobi: " + input[baris][kolom])
            }
        }
        console.log("\n")
    }
}

dataHandling();
console.log("\n")

// Tugas 5
console.log("Jawab no.5")

function balikKata(str) {
    var newString = "";
 
    for (var i = str.length - 1; i >= 0; i--) { 
        newString += str[i];
    }
    return newString;
}
 
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode")) 
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
