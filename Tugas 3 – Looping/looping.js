// Tugas 1
console.log("Tugas No. 1");

var x1 = 2;
var x2 = 20;

console.log("LOOPING PERTAMA");

while (x1 <= 20) {
    h1 = x1 % 2
    if (h1 == 0) {
        console.log(x1 + " - I love coding")
    } 
    x1++;
}

console.log("LOOPING KEDUA");

while (x2 > 0) {
    h2 = x2 % 2
    if (h2 == 0) {
        console.log(x2 + " - I will become a mobile developer")
    } 
    x2--;
}

// Tugas 2 
console.log("Tugas 2");

for (var f = 1; f <= 20; f++) {
    
    var n = f % 2;

    if (n != 0) {
            
        if (f == 3 ) {
            console.log(f + "-I Love Coding")
        }

        else if (f == 9 ) {
            console.log(f + "-I Love Coding")
        }

        else if (f == 15 ) {
            console.log(f + "-I Love Coding")
        }
       
        else {
            console.log(f + "-Santai")
        }    
    }

    else {
        console.log(f + "-Berkualitas")
    }
}

// Tugas 3 
console.log("Tugas 3");

baris = 4
kolom = 8
for (var x = 0; x < baris; x++) {
    var s = ""
    for(var y = 0; y < kolom; y++) {
        s = s + "#";
    }
    console.log(s);
}

// Tugas 4 
console.log("Tugas 4");

baris = 1
kolom = 7
for (var x = 0; x < baris; x++) {
    var s = ""
    for(var y = 0; y < kolom; y++) {
        s = s + "#";
        console.log(s);
    }
}

// Tugas 5 

console.log("Tugas 5");

baris = 8;
kolom = 8;

for (var x = 1; x <= baris; x++) {

    c = x % 2;
    var s = "";

    //Baris Genap
    if (c == 0) {

        for(var y = 1; y <= kolom; y++) {    

            var gogo = y % 2;

            if (gogo == 0) {
                s = s + " ";
            }
           
            else {
                s = s + "#";
            }
        }
        console.log(s);
    }

    //Baris Ganjil
    else if (c =! 0){
        
        for(var y = 1; y <= kolom; y++) {
             
            var gigi = y % 2;

            if (gigi == 0) {
                s = s + "#";
            }
           
            else {
                s = s + " ";
            }
        }
        console.log(s);
    }
}