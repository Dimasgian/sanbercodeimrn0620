//Tugas if-else
var namaPemain="Dimas";
var peranPemain=" "

if (namaPemain != " ") {
    if (peranPemain = " ") {
        console.log("Halo"+namaPemain+",Pilih peranmu untuk memulai game!") 
    }
    else if (peranPemain = "Penyihir") {
        console.log("Selamat datang di Dunia Werewolf," + namaPemain)
        console.log("Halo Penyihir "+namaPemain+",kamu dapat melihat siapa yang menjadi werewolf!")
    }
    else if (peranPemain = "Guard") {
        console.log("Selamat datang di Dunia Werewolf," + namaPemain)
        console.log("Halo Guard"+namaPemain+",kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }
    else if (peranPemain = "Werewolf") {
        console.log("Selamat datang di Dunia Werewolf," + namaPemain)
        console.log("Halo Werewolf"+namaPemain+",Kamu akan memakan mangsa setiap malam!")
    }
}

else {
    console.log("Nama harus diisi!")
}

//Tugas switch
var tanggal = 21; 
var bulan = 1;
var tahun = 1945;

switch (bulan){
    case 1: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " Januari " + tahun);
            }
        } break;
}

    case 2: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " Februari " + tahun);
            }
        } break;
    }

    case 3: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " Maret " + tahun);
            }
        } break;
    }

    case 4: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " April " + tahun);
            }
        } break;
    }

    case 5: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " Mei " + tahun);
            }
        } break;
    }

    case 6: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " Juni " + tahun);
            }
        } break;
    }

    case 7: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " Juli " + tahun);
            }
        } break;
    }

    case 8: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " Agustus " + tahun);
            }
        } break;
    }

    case 9: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " September " + tahun);
            }
        } break;
    }

    case 10: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " Oktober " + tahun);
            }
        } break;
    }

    case 11: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " November " + tahun);
            }
        } break;
    }

    case 12: {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                console.log(tanggal + " Desember " + tahun);
            }
        } break;
    }

    default: {
        console.log("Tanggal, Bulan, atau Tahun, Salah!")
    }
}